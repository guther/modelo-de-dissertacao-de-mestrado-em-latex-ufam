\babel@toc {brazil}{}
\contentsline {chapter}{Agradecimentos}{vi}{chapter*.1}
\contentsline {chapter}{Resumo}{viii}{chapter*.2}
\contentsline {chapter}{\textit {Abstract}}{ix}{chapter*.3}
\contentsline {chapter}{Sum\'ario}{x}{section*.4}
\contentsline {chapter}{Lista de Algoritmos}{xiii}{section*.5}
\contentsline {chapter}{Lista de Códigos}{xiv}{section*.6}
\contentsline {chapter}{Lista de Figuras}{xvi}{section*.7}
\contentsline {chapter}{Lista de Quadros}{xviii}{section*.8}
\contentsline {chapter}{Lista de Tabelas}{xix}{section*.9}
\contentsline {chapter}{Lista de Siglas}{xx}{chapter*.10}
\contentsline {chapter}{\chapternumberline {1}Introdução}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivação e Descrição do Problema}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Objetivos}{3}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Objetivo Geral}{3}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Objetivos Específicos}{3}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Contribuições}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}Estrutura do Documento}{4}{section.1.4}
\contentsline {chapter}{\chapternumberline {2}\textit {JavaScript} e Desenvolvimento \textit {Web}}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Linguagem de Programação \textit {JavaScript}}{6}{section.2.1}
\contentsline {section}{\numberline {2.2}Vazamento de Informação}{11}{section.2.2}
\contentsline {section}{\numberline {2.3}Análise de Marcação (\textit {Taint Analysis})}{12}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Análise de Marcação Dinâmica}{12}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Análise de Marcação Estática}{13}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Termos da Análise de Marcação}{15}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}Considerações Finais}{16}{section.2.4}
\contentsline {chapter}{\chapternumberline {3}Trabalhos Relacionados}{18}{chapter.3}
\contentsline {section}{\numberline {3.1}Análise de Pacotes}{18}{section.3.1}
\contentsline {section}{\numberline {3.2}Lista de Permissão}{20}{section.3.2}
\contentsline {section}{\numberline {3.3}Análise do Fluxo de Informação}{21}{section.3.3}
\contentsline {section}{\numberline {3.4}Discussão}{31}{section.3.4}
\contentsline {chapter}{\chapternumberline {4}\textit {TaintJSec}}{33}{chapter.4}
\contentsline {section}{\numberline {4.1}Identificação e Extração de Código}{34}{section.4.1}
\contentsline {section}{\numberline {4.2}Criação da Árvore Sintática Abstrata}{36}{section.4.2}
\contentsline {section}{\numberline {4.3}Análise do Fluxo da Informação}{37}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Estrutura dos Registradores}{38}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Marcação de Registradores}{39}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}\textit {Taint Sources}}{41}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}\textit {Taint Sinks}}{42}{subsection.4.3.4}
\contentsline {subsection}{\numberline {4.3.5}Propagação do \textit {Taint Tag}}{43}{subsection.4.3.5}
\contentsline {subsubsection}{\numberline {4.3.5.1}Propagação por Atribuição}{44}{subsubsection.4.3.5.1}
\contentsline {subsubsection}{\numberline {4.3.5.2}Propagação por Expressão}{44}{subsubsection.4.3.5.2}
\contentsline {subsubsection}{\numberline {4.3.5.3}Propagação por Passagem de Parâmetro}{47}{subsubsection.4.3.5.3}
\contentsline {subsubsection}{\numberline {4.3.5.4}Propagação por Função Nativa}{47}{subsubsection.4.3.5.4}
\contentsline {subsubsection}{\numberline {4.3.5.5}Propagação na Função Eval (Caso especial de propagação por função nativa)}{48}{subsubsection.4.3.5.5}
\contentsline {section}{\numberline {4.4}Implementação}{53}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Primeira Fase: Identificação e Extração de Código}{54}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Segunda Fase: Árvore Sintática Abstrata}{55}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Terceira Fase: Análise do Fluxo da Informação}{58}{subsection.4.4.3}
\contentsline {section}{\numberline {4.5}Considerações Finais}{65}{section.4.5}
\contentsline {chapter}{\chapternumberline {5}Testes e Resultados}{66}{chapter.5}
\contentsline {section}{\numberline {5.1}Protocolo Experimental}{66}{section.5.1}
\contentsline {section}{\numberline {5.2}Testes de Propagação}{70}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Atribuição}{70}{subsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.1.1}Atribuição Encadeada}{70}{subsubsection.5.2.1.1}
\contentsline {subsubsection}{\numberline {5.2.1.2}Atribuição Simples}{71}{subsubsection.5.2.1.2}
\contentsline {subsection}{\numberline {5.2.2}Condicional}{74}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Função}{77}{subsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.3.1}Função Não-Nativa}{77}{subsubsection.5.2.3.1}
\contentsline {subsubsection}{\numberline {5.2.3.2}Função Nativa}{80}{subsubsection.5.2.3.2}
\contentsline {subsection}{\numberline {5.2.4}\textit {Prototype}}{82}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}Laço de Repetição}{90}{subsection.5.2.5}
\contentsline {subsubsection}{\numberline {5.2.5.1}\textit {Do While}}{90}{subsubsection.5.2.5.1}
\contentsline {subsubsection}{\numberline {5.2.5.2}\textit {For}}{92}{subsubsection.5.2.5.2}
\contentsline {subsubsection}{\numberline {5.2.5.3}\textit {For In}}{93}{subsubsection.5.2.5.3}
\contentsline {subsubsection}{\numberline {5.2.5.4}\textit {While}}{94}{subsubsection.5.2.5.4}
\contentsline {subsubsection}{\numberline {5.2.5.5}\textit {Switch}}{95}{subsubsection.5.2.5.5}
\contentsline {subsubsection}{\numberline {5.2.5.6}\textit {Try-Catch}}{96}{subsubsection.5.2.5.6}
\contentsline {subsubsection}{\numberline {5.2.5.7}Vetor}{98}{subsubsection.5.2.5.7}
\contentsline {section}{\numberline {5.3}Testes com a Função \textit {eval}}{101}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Função \textit {eval} + Ofuscação de código + Propagação nos objetos do DOM}{102}{subsection.5.3.1}
\contentsline {section}{\numberline {5.4}Testes em Códigos Ofuscados}{104}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}\textit {JSCompress}}{104}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}\textit {Aaencode}}{106}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}\textit {JSFuck}}{109}{subsection.5.4.3}
\contentsline {subsection}{\numberline {5.4.4}\textit {JSCrambler}}{112}{subsection.5.4.4}
\contentsline {subsection}{\numberline {5.4.5}\textit {Packer}}{116}{subsection.5.4.5}
\contentsline {chapter}{\chapternumberline {6}Conclusão}{118}{chapter.6}
\contentsline {section}{\numberline {6.1}Considerações Finais}{118}{section.6.1}
\contentsline {section}{\numberline {6.2}Lições Aprendidas}{119}{section.6.2}
\contentsline {section}{\numberline {6.3}Trabalhos Futuros}{121}{section.6.3}
\contentsline {chapter}{Refer\^encias Bibliogr\'aficas}{123}{section*.12}
