\contentsline {lstlisting}{\numberline {1.1}Exemplo de código \textit {JavaScript} utilizando a função \textit {eval}}{2}{lstlisting.1.1}
\contentsline {lstlisting}{\numberline {2.1}Exemplo de \textit {JavaScript inline}.}{7}{lstlisting.2.1}
\contentsline {lstlisting}{\numberline {2.2}Exemplo de \textit {JavaScript} externo.}{8}{lstlisting.2.2}
\contentsline {lstlisting}{\numberline {2.3}Trecho de código explícito contendo códigos implícitos.}{8}{lstlisting.2.3}
\contentsline {lstlisting}{\numberline {2.4}Trecho de código explícito contendo códigos implícitos ofuscados.}{10}{lstlisting.2.4}
\contentsline {lstlisting}{\numberline {4.1}Exemplo de HTML sem código \textit {JavaScript}.}{35}{lstlisting.4.1}
\contentsline {lstlisting}{\numberline {4.2}Código contendo objetos considerados \textit {taint sinks} pelo \textit {TaintJSec}.}{43}{lstlisting.4.2}
\contentsline {lstlisting}{\numberline {4.3}Exemplo de uso do módulo jsDom, utilizando a biblioteca \textit {jQuery} para manipular objetos do código HTML.}{54}{lstlisting.4.3}
\contentsline {lstlisting}{\numberline {4.4}Função de extração de trechos scripts utilizando \textit {jQuery}.}{54}{lstlisting.4.4}
\contentsline {lstlisting}{\numberline {4.5}Exemplo de código \textit {JavaScript} com sintaxe JSX.}{56}{lstlisting.4.5}
\contentsline {lstlisting}{\numberline {4.6}Classe de registradores de objetos.}{63}{lstlisting.4.6}
\contentsline {lstlisting}{\numberline {4.7}Função para criar escopos.}{63}{lstlisting.4.7}
\contentsline {lstlisting}{\numberline {4.8}Exemplo da utilização do \textit {prototype} para inserir novos atributos em uma função/classe.}{64}{lstlisting.4.8}
\contentsline {lstlisting}{\numberline {5.1}Exemplo de um código \textit {JavaScript} que usa informação sensível como chave de vetor para praticar o vazamento de informação.}{67}{lstlisting.5.1}
\contentsline {lstlisting}{\numberline {5.2}Teste de atribuição encadeada.}{70}{lstlisting.5.2}
\contentsline {lstlisting}{\numberline {5.3}Teste de atribuição simples}{72}{lstlisting.5.3}
\contentsline {lstlisting}{\numberline {5.4}Resultado do teste de condicional}{75}{lstlisting.5.4}
\contentsline {lstlisting}{\numberline {5.5}Teste de função não-nativa}{78}{lstlisting.5.5}
\contentsline {lstlisting}{\numberline {5.6}Resultado do teste de função nativa}{81}{lstlisting.5.6}
\contentsline {lstlisting}{\numberline {5.7}Resultado do teste de \textit {prototype}}{84}{lstlisting.5.7}
\contentsline {lstlisting}{\numberline {5.8}Resultado do teste do laço de repetição \textit {Do While}}{91}{lstlisting.5.8}
\contentsline {lstlisting}{\numberline {5.9}Resultado do teste do laço de repetição \textit {For}}{92}{lstlisting.5.9}
\contentsline {lstlisting}{\numberline {5.10}Resultado do teste do laço de repetição \textit {For In}}{93}{lstlisting.5.10}
\contentsline {lstlisting}{\numberline {5.11}Resultado do teste do laço de repetição \textit {While}}{94}{lstlisting.5.11}
\contentsline {lstlisting}{\numberline {5.12}Resultado do teste de \textit {switch}}{95}{lstlisting.5.12}
\contentsline {lstlisting}{\numberline {5.13}Resultado do teste de \textit {Try-Catch}}{97}{lstlisting.5.13}
\contentsline {lstlisting}{\numberline {5.14}Resultado do teste com Vetor}{99}{lstlisting.5.14}
\contentsline {lstlisting}{\numberline {5.15}Código da página \textit {getDados.php}.}{102}{lstlisting.5.15}
\contentsline {lstlisting}{\numberline {5.16}Trecho do código da aplicação maliciosa.}{103}{lstlisting.5.16}
