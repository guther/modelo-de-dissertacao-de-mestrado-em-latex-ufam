\chapter{Outros Pontos}\label{outros}

É absolutamente impossível cobrir todo o material necessário para se escrever uma boa monografia em um único documento. A melhor prática é consultar material específico (por exemplo, livros de gramática e de metodologia científica) e praticar a escrita \citep{BoothCW08,GraffB09,Turabian07,Wazlawick09,Zobel04}.
De qualquer modo, esta seção apresenta alguns pontos extra que podem auxiliar nessa difícil tarefa. Eles não seguem nenhuma ordem de importância.

\section{Estilo, Conteúdo e Afins}

\ponto{Figuras e Tabelas}  
Lembre-se de sempre referenciar figuras e tabelas no seu texto. Por exemplo, a Figura \ref{fig:exemplo1} apresenta um exemplo de como fica uma figura qualquer, e a Tabela \ref{tab:exemplo1} como é uma tabela simples em tex. %Veja o código tex para saber como referenciar tabelas e figuras usando \verb|\label| e \verb|\ref|. 


\begin{figure}[tb]
    \centering
    \includegraphics{img/exemplo}
    %\includegraphics[scale=.5]{exemplo-x} % caso queira mudar o tamanho da figura
    \caption{Uma figura de exemplo.}
    \label{fig:exemplo1}
\end{figure}


\begin{table}[tb]
    \caption{Uma tabela de exemplo.}
    \label{tab:exemplo1}
    {\centering
    \begin{tabular}{lcr} \toprule
    \emph{Left-aligned}  &  \emph{Centered}  &  \emph{Right-aligned} \\ \midrule
    Lorem ipsum  &  dolor sit  &  amet \\
    consectetur adipisicing  &  elit, sed do eiusmod  &  tempor \\
    incididunt ut  &  labore et dolore  &  magna aliqua. \\ \bottomrule
    \end{tabular}\par
    }
\end{table}



\dica{Por questão de estilo, figuras e tabelas ficam mais legíveis no começo ou no final da página. Veja o código tex para saber como fazê-lo utilizando a opção [tb].}


\ponto{Revisão Final} 
Lembre-se de realizar uma revisão bem apurada antes de passar seu trabalho adiante. Verifique os seguintes pontos:

\begin{itemize}
	\item Ortografia: título, nomes dos autores e filiação; 
	\item Ortografia: passe um corretor ortográfico (\textit{spell checker}) em todo o trabalho;
	\item Legibilidade: imprima o trabalho (no formato final de submissão);
	\item MS Word: numeração das seções e subseções, numeração no texto concorda com a numeração usada em figuras e tabelas, referências cruzadas não foram perdidas dentro do editor.  
\end{itemize}

\ponto{Sete Pecados Capitais} 
Estilo é de cada um, mas cuidado com o seguinte:

\begin{enumerate}
	\item Frases longas (repletas de vírgulas ou não!); 
	\item Erros ortográficos; 
	\item Tradução literal (\textit{tea with me}) e \textit{imbromation} (escrever em inglês palavras que não existem);
	\item Imagens/tabelas ilegíveis; 
	\item Erros gramaticais (paralelismo, concordância, conjugação, crase);  
	\item Cópia literal; 
	\item Blablabla (encher linguiça). 
\end{enumerate}

\ponto{Divisão do texto} 
Cada parágrafo deve ter frases de abertura e encerramento indicando o propósito do parágrafo. Uma seção formada apenas por \textit{bullets} (como algumas deste documento) não \textbf{(NÃO!)} é aceitável em um trabalho científico. De mesmo modo, evite subseções que contêm apenas um parágrafo. Via de regra, uma seção (um capítulo) é formada por mais de um parágrafo, o qual é formado por várias frases. Uma seção apenas com uma lista de itens não é uma seção, e sim uma lista de itens.

A Figura \ref{fig:paragrafos}a apresenta um exemplo fictício para a \textit{má} divisão de seções. Veja que o texto desse exemplo contém inúmeros outros problemas, entre eles: 

\begin{figure}[bt]
    \centering
    \includegraphics[scale=.8]{img/paragrafo} % caso queira mudar o tamanho da figura
    \caption{\textit{Mau} exemplo de divisão de seções e sua correção}
    \label{fig:paragrafos}
\end{figure}



\begin{itemize}
	\item Frases longas, incompreensíveis e que enchem linguiça sem dizem absolutamente nada de útil (primeira frase); 
	\item Seção começa direto com uma subseção. Cada seção deveria começar com pelo menos um parágrafo (para introduzir o conteúdo das subseções);
	\item Erro de crase (tolerante à falhas: ou é ``às falhas'', o qual não faz o menor sentido, ou é ``à falha'' ou ``a falhas'');
	\item Utilização de primeira pessoa (apresentamos)  pode ser trocada pelo impessoal: ``Essa seção apresenta o Índice Preguiçoso.''
	\item Evitar ``abaixo'' e ``acima'', pois não se pode garantir que essas informações estarão apresentadas na mesma página. Se for estritamente necessário, utilize ``a seguir'' e ``anterior'';
	\item Toda a figura DEVE ser referenciada pelo seu  número, bem como devidamente explicada no texto;
	\item SQL, Sql ou sql? Como é uma sigla, utilize sempre a primeira opção (tudo em maiúsculo). Mais importante, utilize sempre da mesma forma em 
todo o texto;
	\item Evite termos em inglês quando existe um termo técnico em português (\textit{performance} é desempenho). De mesmo modo, evite traduzir pessoalmente conceitos que só existem em inglês. Se for inevitável, coloque o termo original em inglês entre parênteses e itálico.
\end{itemize}

Uma versão muito melhor do mesmo texto é apresentada na Figura \ref{fig:paragrafos}b. 

\ponto{Cópia Literal} Via de regra, em Computação, cópia literal \textit{não} é aceita. Quando referenciar outros trabalhos, resuma suas ideias principais. Resista à tentação de copiar literalmente colocando o texto entre ``...'', pois apesar desse tipo de citação ser muito comum em textos de Literatura e outras áreas, em Computação não é. 

\ponto{Figura ou Tabela} Geralmente, se os dados mostram uma tendência, criando uma ilustração interessante, faça uma figura. Se os números apenas estão lá, sem qualquer tendência interessante em evidência, uma tabela deveria ser suficiente. Lembre-se que tabelas também são preferíveis para apresentar números exatos. Tanto figuras quanto tabelas devem ser mencionadas no texto sendo que: figuras possuem explicação detalhada no texto  e tabelas podem ser autossuficientes. Lembre-se que uma imagem vale mil palavras. Desse modo, se o trabalho apresenta um processo complicado ou uma nova arquitetura, cheio de fases, entradas e saídas, fluxos de dados, etc, tente resumir tudo em uma imagem.  

\ponto{Qualidade do Texto} A qualidade do texto científico é garantida por vários fatores. Especificamente, gírias são inadmissíveis, assim como ironias, brincadeiras e referências pessoais ao leitor. Deve também existir consistência na utilização do tempo verbal (deve-se evitar utilizar passado, presente e futuro indiscriminadamente). Palavras no estrangeiro devem ser grifadas em itálico. Todas as siglas esclarecidas, pois pode existir sobreposição de termos. Por exemplo, ``...conforme definido pela W3C (\textit{World Wide Web Consortium})''.


\ponto{Tempo verbal}
 Via de regra, o tempo verbal do seu trabalho é o tempo presente (tanto para textos em português quanto inglês). Exceções: pode-se utilizar o tempo passado ao mencionar trabalhos realizados anteriormente pelos mesmos autores ou outros (ex: Ragavhan mostrou que...), pode-se utiliza o passado nas seções finais de cada capítulo (ex: Este capítulo apresentou as definições...) e nas conclusões (ex: Este trabalho apresentou uma nova forma de ...); pode-se utilizar o tempo futuro apenas ao referenciar trabalhos que serão realizados, por exemplo, na conclusão ao mencionar trabalhos futuros (ex: Como trabalhos futuros, serão definidas novas formas para ...). 

\ponto{Linhas e Palavras Órfãs}
É muito comum que o próprio editor divida parágrafos e linhas nas páginas.
Igualmente comum é que o editor deixe linhas e palavrás órfãs nas páginas e parágrafos.
Ou seja: dividir as palavras no parágrafo e deixar uma ou duas palavras órfãs na última linha do mesmo, ou dividir os parágrafos e deixar uma linha órfã no início da página seguinte. 
Nesses casos, é de bom estilo reduzir ou expandir o parágrafo para evitar a palavra/linha extra.



\ponto{Backup! Backup! Backup! Backup! Backup!} Seguro morreu de velho. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Português Rococó + Inglês Imbromation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Tanto língua portuguesa quanto inglesa são lindas e cheias de eufemismos maravilhosos e rococós para enfeitar seu texto\footnote{Seção retirada de \textit{Como condensar artigos}, \url{http://www.dcc.ufmg.br/~mirella/doku.php?id=alunos}}. Como digo aos meus filhos (tanto os biológicos quanto os acadêmicos): NA NI NÃO. O texto científico acima de tudo tem de ser claro e objetivo (repita comigo: \textbf{claro e objetivo, claro e objetivo, claro e objetivo}). Deixe os eufemismos e rococós, assim como adjetivos e advérbios supérfluos, para quem precisa (o povo de humanas, por exemplo). A Tabela \ref{tab:rococo} apresenta alguns exemplos (especial atenção para os rococós em vermelho - porque não vou perder tempo listando ``pleonasmos redundantes'').\footnote{Exemplos reais de textos escritos pelos meus queridos orientados, que desde então estão escrevendo muito melhor.}

\begin{table}[tb]
    \caption{Correção para os rococós mais comuns em inglês.}
    \label{tab:rococo}
		\scriptsize
    {\centering
    \begin{tabular}{p{9cm}p{6cm}} \toprule
    \emph{TEXTO ORIGINAL com rococó comedor de espaço}  &  \emph{Sugestão} \\ \midrule
This approach \pink{takes into consideration}$^a$ ABC ...& This approach considers ABC ...\\ \hline 
Table 5.9 \pink{presents a summary of all the} methods ...& Table 5.9 summarizes all methods ...\\ \hline 
\pink{In the literature$^b$, one may} find studies over ABC ...& There are studies over ABC ...\\ \hline 
...AB and CD, which are applied \pink{on literature} to capture the ... &  ...AB and CD, which capture the...\\ \hline 
\pink{Basically}$^c$, the new algorithm provides wonderful advantages regarding time, space and complexity. & The algorithm outperforms existing ones on time, space and complexity.\\ \hline 
...A is better than B. \pink{This happens due to the fact that} A is ... &  ...A is better than B, because A is ...\\ \hline 
The results are in Figure 1. \pink{Obviously}$^d$, \pink{one can see} that A is better than B. & Figure 1 shows the results, with A being better than B.\\ \hline 
The results are in Figure 1. \pink{It is interesting to see that} A is better than B. We wonder that the results are like these because A ... &  Figure 1 shows the results, with A being surprisingly better than B, mostly because A ...\\ \hline 
Such items \pink{may take quite varied forms, for example,} reviews, places, books, movies, news, music, videos, ads, websites, products from a virtual store, among others [11].  &  Examples of items include reviews, places, books, movies, news, among others [11].\\ \hline 
We focus on studying ABC ... &  We study ABC ...\\ \hline 
...the properties that we use in our analysis (Section 5). & ...the properties used in Section 5.\\ \hline 
Such metric is used to show that A is way better ...& Such metric shows A is better ...\\ \hline 
COMO NOTA DE RODAPÉ: Readers can easily access our dataset in github.com/xyz & Dataset available at github.com/xyz.\\ \hline 
These constraints are concerned with A, B, and C & These constraints concern A, B and C\\ \hline 
An example of this constraint is the rule that does not allow a building to be intercepted by a street segment. & An example is: a building cannot be intercepted by a street segment.\\ \hline 
...so as to promote a great deal of reading and writing operations per second & ...to support many reading and writing operations per second. \\ \hline 
NoSQL databases have four different data storage architectures to manipulate different data types and, therefore, it is proper to different usage scenarios. They are: Key-Value, Column-Oriented, Document-Oriented and Graph-Oriented & There are four architectures to handle NoSQL data: key-value, column-oriented, document-oriented and graph-oriented. \\ \hline 
In this section, our goal is to evaluate the performance...& We now evaluate the performance...\\ \hline 
Figure 1 illustrates that our algorithm ...& Figure 1 shows our algorithm...$^e$\\ \hline
The experimental evaluation we are seeking emphasize time, space and memory consumption. & The experimental evaluation emphasizes time, space and memory consumption.$^f$ \\ \hline
Figure 2.4 shows an example, with users represented in lines and items, which are movies in the example, in columns. & Cuidado com as vírgulas-rococós: Figure 2.4 shows an example with users as lines and items (movies) as columns.   
		\\ \bottomrule
		\multicolumn{2}{p{15cm}}{$^a$ ESQUEÇA que existe ``take into consideration'' e a equivalente ``levamos em consideração'', e utilize o verbo existente (to consider, considerar). O exemplo seguinte serve para inglês e português: ``A Tabela apresenta um resumo'', ``A Tabela resume''.} \\
		\multicolumn{2}{p{15cm}}{$^b$ POR FAVOR: estamos em Ciência da Computação (ou outra disciplina das Exatas), esqueça a palavra \textit{literatura} meu filho, ela é óbvia! É óbvio que você encontrou os demais artigos na literatura assim como o ar é cheio de O$^2$.} \\
		\multicolumn{2}{p{15cm}}{$^c$ BASICALLY deveria ser retirada de qualquer vocabulário científico que se preze porque é a palavra mais vazia do mundo! É a mesma coisa que escrever no artigo ``Tipo assim, o novo algoritmo...'' Aaaargh, em algum lugar no mundo, uma fada científica acaba de morrer.} \\
		\multicolumn{2}{p{15cm}}{$^d$ Se é óbvio, então não é ciência :-/} \\
		\multicolumn{2}{p{15cm}}{$^e$ IMPORTANTES: (1) evite palavras longas se existem sinônimos com menos caracteres (2) 90\% das vezes a palavra that pode ser eliminada sem prejuízo à frase} \\
		\multicolumn{2}{p{15cm}}{$^f$ Veja que ``we are seeking'' não faz sentido na frase, assim como a maioria dos gerúndios verbais utilizados.} \\
    \end{tabular}\par
    }
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Referências Bibliográficas}\label{intro:referencia}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% TEXTO ORIGINAL:
O pacote \verb|natbib| permite uma série de formas diferentes para fazer referências bibliográficas. O comando padrão \verb|\cite| realiza a citação comum, como por exemplo \cite{BoothCW08,Zobel04}. Outros comandos permitem  citar somente o autor --- por exemplo, citar o trabalho de \citeauthor{Wazlawick09} --- ou colocar automaticamente todas as citações entre um único par de colchetes \citep{GraffB09,Turabian07}. Os comandos usados foram, respectivamente, \verb|\citeauthor| e \verb|\citep|. Veja a documentação do \verb|natbib|  para conhecer outros comandos e exemplos de uso.

%Citações aleatórias para fazer com que as referências bibliográficas ocupem mais de uma página: \cite{bichsel92simple, dror01statistics, guisser92new}.

% POR MIRELLA M. MORO

As referências são muito importantes para qualquer trabalho científico. Elas fornecem o embasamento necessário para entendê-lo bem como bibliografia complementar ao seu conteúdo. Considere as seguintes observações para as 
referências: 
\begin{itemize}
	\item São corretas, completas, específicas; 
	\item Possuem informações obrigatórias: autores, título, nome do 
evento ou periódico (editora), volume e número se 
necessário, ano; 
	\item Incluem referências relevantes:
		\begin{itemize}
		\item Do mesmo ano  (ou ano anterior) para ilustrar que o 
tópico é atual e de interesse da comunidade; 
		\item Artigos de  conferências,  periódicos,  livros (não 
apenas sites da Internet!);  
		\item Todas as obras listadas no conjunto de referências 
devem ser mencionadas no texto, e vice-versa. 
		\end{itemize}
\end{itemize}

Veja o exemplo a seguir. \textit{Segundo \cite{Horn86}, todo triângulo equilátero tem os lados iguais. Já segundo \cite{Shashua97}, todo quadrado também tem.}

Um ponto final é como mencionar as referências no texto. Por exemplo, qual é a melhor maneira de referenciar o trabalho [1]?

\vspace{6pt}
{\sffamily
\noindent a. [1] informa que ...\\
b. [1] informou que ...\\
c. Moro et al informam que ...[1]. \\
d. Os autores em [1] informam/informaram que ...\\
e. Uma frase com a definição ou informação completa [1]. \\
f. Uma frase com a definição ou informação completa, conforme definido por [1].
}\vspace{6pt}


Claramente, as opções \textit{a} e \textit{b} são estranhas porque a referência por si só não informa nada (é uma questão de estilo). O mais apropriado seria referenciar os autores que realizaram o trabalhando informando algo útil, conforme as opções \textit{c} e \textit{d}. Outras formas mais comuns são apresentadas em \textit{e} e \textit{f}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{LaTeX}\label{intro:latex}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Caso o autor não estiver familiarizado com LaTeX e precisar usá-lo, sugere-se o seguinte.

\textbf{LaTeX no Windows}: vários alunos resistem à utilização do LaTeX porque adoram usar o MS Word ou BrOffice no Windows. É possível continuar usando Windows com o LaTeX. Especificamente, pode-se utilizar um editor LaTeX online (exemplos incluem \url{http://www.sharelatex.com} e \url{http://www.overleaf.com}) ou instalá-lo através de três passos simples:

\begin{enumerate}
	\item Instalar o LaTeX. Ir ao site do Miktex Project: http://miktex.org\footnote{\textit{About MiKTeX: MiKTeX (pronounced mick-tech) is an up-to-date implementation of TeX and related programs for Windows (all current variants). TeX is a typesetting system written by Donald E. Knuth, who says that it is ``intended for the creation of beautiful books - and especially for books that contain a lot of mathematics''.}} 
	\item No site do Miktex, fazer download MiKTeX versão atual \textit{Net Installer}. Rodar o programa \textbf{duas vezes}: primeiro para fazer DOWNLOAD e depois para INSTALAR o programa. Instalar a versão completa do miktex para evitar problemas futuros de falta de arquivos. \textbf{Importante}: a instalação do miktex geralmente leva hooooras, sugere-se ter paciência  e evitar interromper a instalação.
	\item Instalar interface gráfica para o LaTeX. Pode-se utilizar o TeXnicCenter\footnote{TeXnicCenter: \url{http://www.texniccenter.org} -- Este é um exemplo de como referenciar páginas Web: simplesmente coloque-as como nota de rodapé (muito melhor do que ficar enchendo a seção de Referências com websites).}, o qual foi utilizado para construir este documento.
\end{enumerate}

Além disso, a leitura do Anexo \ref{anexo:latex} é praticamente obrigatória nesse caso.