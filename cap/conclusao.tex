\chapter{Conclusão}\label{conclusao}

Este capítulo apresenta as conclusões deste trabalho de pesquisa, as limitações e as contribuições para a comunidade científica. Além de possíveis trabalhos futuros, que visam sanar limitações da análise.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Considerações Finais}\label{conclusao:consideracoesfinais}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Este trabalho apresentou o \textit{TaintJSec}, uma abordagem de análise estática, para a detecção de vazamento de informação sensível em código \textit{JavaScript}. Diferente das abordagens existentes, o \textit{TaintJSec} é capaz de verificar código \textit{JavaScript} explícitos e implícitos. Mais ainda, o \textit{TaintJSec} consegue analisar a propagação de um dado sensível (\textit{taint data}) na execução da função \textit{eval}, a qual é bastante utilizada em técnicas de ofuscação de código. Além de conseguir identificar o vazamento de informação sensível em códigos ofuscados por ferramentas criadas especificamente para tal finalidade. 

A avaliação do \textit{TaintJSec} utilizando 14 conjuntos de testes mostraram que a acurácia da abordagem proposta é superior às ferramentas \textit{JPrime}, \textit{ScanJS}, \textit{JSpwn}, \textit{KSLint}, \textit{Jalangi} e \textit{jsTaint}.


Entretanto, apesar da abordagem \textit{TaintJSec} ter obtido bons resultados, há muitas questões a serem melhoradas, como por exemplo:

\begin{itemize}

\item não verifica todos os caminhos possíves do fluxo de execução. De tal modo que uma operação condicionante \textit{IfStatement} checa somente o escopo verdadeiro se a condição for verdadeira, ou checa o escopo falso se a condição for falsa;
\item não analisa o comportamento dos \textit{Event Listeners}. Logo, funções executadas por meio de eventos são ignoradas;
\item não tem suporte a \textit{callbacks} como o objeto \textit{new Proxy};
\item não tem suporte a função geradora;
\item não tem suporte ao modo estrito (diretiva \textit{"use strict"}).
\item não tem suporte a códigos \textit{JavaScript} com comportamento assíncrono.

\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Lições Aprendidas}\label{conclusao:licoesaprendidas}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Durante a implementação do \textit{TaintJSec} surgiram alguns problemas, os quais foram sanados após nova fase de planejamento e pequisa. Esses problemas apresentavam-se como limitações da plataforma \textit{Node.js}, limitações nos módulos escolhidos para as fases da abordagem ou alguma característica da linguagem \textit{JavaScript} que não havia sido prevista.

Nesta seção são descritos os problemas enfrentados durante este trabalho de pesquisa e as medidas utilizadas para contorná-los.

\vspace{3mm}\medskip\textbf{Limitações do \textit{Node.js}}

O microprocessador possui grande parte das funções nativas do \textit{JavaScript} e, por isso dispensa o trabalho de criação das mesmas. Porém, não fornece suporte às funções e métodos nativos presentes nos navegadores.  

No mundo real, a possibilidade de encontrar um código \textit{JavaScript} que não utilize alguma função ou método que só existem nos navegadores é muito baixa. São exemplo de objetos que o \textit{Node.js} não oferece suporte são Audio e Video.

Para contornar essas limitações foram utilizados os objetos \textit{document} e \textit{window} fornecidos pelo módulo jsDom. Desse modo, foi possível identificar a existência de métodos exclusivos do HTML e não da linguagem \textit{JavaScript}. Para o objeto Audio foi utilizado o objeto \textit{window.Audio}.

Contudo, os objetos que não eram fornecidos ou emulados pelos módulos do \textit{Node.js} ainda causavam problemas durante a análise, como o objeto \textit{navigator.userAgent}, que não é fornecido pela versão mais recente do \textit{Node.js} e nem pelo módulo jsDom. Para resolver esse problema, tais objetos podem ser emulados por meio da criação de um objeto \textit{navigator} com o atributo \textit{userAgent} e um valor literal fixo.


\vspace{3mm}\medskip\textbf{Cheerio x jsDom}

Cheerio\footnote{ \url{https://github.com/cheeriojs/cheerio}} é um módulo do \textit{Node.js}, que fornece um subconjunto de operações da biblioteca \textit{jQuery} possibilitando o acesso ao DOM de códigos HTML mesmo em \textit{server-side}.

Foi desenvolvido como uma alternativa ao módulo jsDom e, segundo seus criadores, é até oito vezes mais rápido que o jsDom. Porém, objetos criados dinamicamente a partir de operações como \textit{document.createElement} não são adicionados ao DOM gerado pelo Cheerio e, devido a isso, ocorreram erros durante a análise de código. Essa limitação tornou inviável a utilização desse módulo durante a implementação do \textit{TaintJSec}. Em substituição, foi utilizado o jsDom. 

\vspace{3mm}\medskip\textbf{Contexto de Escopo em \textit{JavaScript}}

A linguagem \textit{JavaScript} executa um contexto de escopo em dois tempos. No primeiro momento são verificadas todas as possíveis declarações de variáveis e funções. Para cada operação de declaração de variável é atribuído o valor \textit{undefined} à variável, e toda declaração de função criada pelo usuário é interpretada, mas não executada. 

A Figura \ref{fig_momentos_exec_escopo} apresenta um exemplo de código \textit{JavaScript} e os dois momentos da leitura do código. No primeiro momento, é interpretada somente a operação de declaração de variável na linha 3, mas sem a atribuição. O valor adicionado para a variável b é \textit{undefined}.

No segundo momento, as demais operações são interpretadas. Então, a operação de atribuição é executada e o valor de b é atribuído à variável a. 

Um programador desatento poderia imaginar que o código apresentaria erro devido a atribuição da linha 1, onde a variável b não existe. Contudo, a linha 1 só é executada no segundo momento da interpretação das operações do escopo, quando a variável b passaria a existir após o primeiro momento da interpretação das operações.

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.5]{img/fig_momentos_exec_escopo.png}
    \caption{Exemplo de código \textit{JavaScript} e os dois momentos de execução do escopo.}
    \label{fig_momentos_exec_escopo}
\end{figure}

Para implementar essa característica da linguagem \textit{JavaScript}, o framework SAFE realiza uma tarefa de organização da estrutura do código de entrada, colocando as operação de declaração de função e declaração de variável antes de qualquer outra operação do código. Somente quando o código está reestruturado é que o analisador do SAFE é executado. A Figura \ref{fig_trans_code_safe} apresenta um exemplo de como o SAFE reescreve o código \textit{JavaScript}, movendo todas as operações de declaração de função para o início do código, e abaixo delas insere as operações de declaração de variável sem a atribuição de valores.

\begin{figure}[h]
    \centering
    \includegraphics[scale=.5]{img/fig_trans_code_safe.png}
    \caption{Exemplo de um código \textit{JavaScript} e a sua versão reestruturada pelo SAFE.}
    \label{fig_trans_code_safe}
\end{figure}

A coluna Código Original da Figura \ref{fig_trans_code_safe} apresenta na linha 2 e linha 3 operações de declaração de variável e função, respectivamente. Essas operações são movidas para o início do código \textit{JavaScript} formando um código reestruturado. Primeiro a declaração de função é inserida na linha 1 do  Código Reestruturado e logo em seguida, na linha 2 é inserida a operação de declaração de variável, sem o valor de atribuição. Com o código reestruturado, o SAFE pode analisar o fluxo de execução do código original da mesma maneira como um interpretador de \textit{JavaScript} do mundo real o faria.

O \textit{TaintJSec} utiliza uma abordagem diferente do SAFE para implementar essa execução de escopo em dois momentos. Em vez de reestruturar o código original, o \textit{TaintJSec} analisa a AST duas vezes. Na primeira análise da AST, somente as declarações de função e variável são inseridas nos registradores. Os registradores armazenam as variáveis declaradas e atribuem o valor \textit{undefined} a elas. Todas as demais operações da AST não são inseridas nos registradores, mas são analisadas para que novas operações de declaração de variável possam ser encontrada dentro do escopo de operações como \textit{IF}, \textit{Try-Catch} e laços de repetição.

Quando a primeira análise da AST chega ao fim, os registradores de objetos contém todas as funções declaradas no escopo e todas as variáveis declaradas estão com o valor \textit{undefined}. Então é iniciada a segunda análise da AST, contabilizando e inserindo os valores obtidos nas demais operações.

Assim, a característica da linguagem \textit{JavaScript} de executar o contexto duas vezes é respeitada e todas as computações ocorrem corretamente.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Trabalhos Futuros}\label{conclusao:trabalhos_futuros}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Neste trabalho foram identificadas as seguintes possibilidades de trabalhos futuros:
\begin{enumerate}

\item A inclusão completa do DOM na análise realizada, podendo reconhecer métodos próprios dos navegadores, os quais não existem por padrão no \textit{Node.js} e nem em módulos adicionais, tais como \textit{navigator.userAgent};
\item Estender a \textit{taint propagation} pelos objetos do HTML para permitir a marcação de \textit{tags} HTML que possuam dados sensíveis no valor de pelo menos um de seus atributos ou em seu conteúdo \textit{innerHTML};
\item A criação de um controle de \textit{taint tags} para os objetos do tipo \textit{array}, pois atualmente existe somente um \textit{taint tag} para o objeto inteiro, de tal maneira que esse controle poderia observar a propagação do \textit{taint tag} por posição do objeto, diminuindo o número de falsos positivos.
\end{enumerate}