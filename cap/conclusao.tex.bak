\chapter{Conclusão}\label{conclusao}

Este capítulo apresenta as conclusões deste trabalho de pesquisa, as limitações e as contribuições para a comunidade científica. Além de possíveis trabalhos futuros, que visam sanar limitações da análise.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Considerações Finais}\label{conclusao:consideracoesfinais}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Este trabalho apresentou o \textit{TaintJSec}, uma abordagem de análise estática, para a detecção de vazamento de informação sensível em código \textit{Javascript}. A abordagem apresentada verifica o fluxo explícito de código e também o fluxo implícito, consegue analisar a propagação de um \textit{taint data} na execução da função \textit{eval}, que é deveras utilizada em técnicas de ofuscação de código. Além de conseguir identificar o vazamento de informação sensível em códigos ofuscados por ferramentas criadas especificamente para tal finalidade, o \textit{TaintJSec} não compromete o desempenho das aplicações por realizar unicamente a análise estática, sendo essa a menos onerosa de todas as técnicas de análise de fluxo existente no estado-da-arte.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Contribuições}\label{conclusao:contribuicoes}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Este trabalho forneceu as seguintes contribuições:

\begin{enumerate}

\item Um analisador estático de código \textit{Javascript} capaz de identificar o vazamento de informação sensível em códigos implícitos e ofuscados;
\item Uma técnica denominada \textit{taint position} para aumentar o controle da propagação do \textit{taint flag} na análise da função \textit{eval};
\item Um conjunto de testes organizado em 13 grupos distintos, onde cada grupo avalia o poder computacional do analisador e a correta propagação do \textit{taint flag} no fluxo de execução.

\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Limitações}\label{conclusao:limitacoes}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

As limitações do \textit{TaintJSec} são apresentadas abaixo. A abordagem:

\begin{itemize}

\item não verifica todos os caminhos possíves do fluxo de execução. De tal modo que uma operação condicionante \textit{IfStatement} checa somente o escopo verdadeiro se a condição for verdadeira, ou checa o escopo falso se a condição for falsa;
\item não analisa o comportamento dos \textit{Event Listeners}. Logo, funções executadas por meio de eventos são ignoradas;
\item não tem suporte a \textit{callbacks} como o objeto \textit{new Proxy}, por exemplo.

\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Trabalhos Futuros}\label{conclusao:trabalhos_futuros}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Neste trabalho foram identificadas as seguintes possibilidades de trabalhos futuros:
\begin{enumerate}

\item A inclusão completa do DOM na análise realizada, podendo reconhecer métodos próprios dos navegadores, os quais não existem por padrão no \textit{Node.js} e nem em módulos adicionais, tais como \textit{navigator.userAgent};
\item Estender a \textit{taint propagation} pelos objetos do HTML para permitir a marcação de \textit{tags} HTML que possuam dados sensíveis no valor de pelo menos um de seus atributos ou em seu conteúdo \textit{innerHTML};
\item A criação de um controle de \textit{taint flags} para os objetos do tipo \textit{array}, pois atualmente existe somente um \textit{taint flag} para o objeto inteiro, de tal maneira que esse controle poderia observar a propagação do \textit{taint flag} por posição do objeto, diminuindo o número de falsos positivos.
\end{enumerate}