\chapter{Introdução}\label{introducao}

A linguagem \textit{JavaScript} é atualmente uma das linguagens de programação mais populares e mais utilizadas no mundo para a criação de aplicações \textit{web} \cite{7476665,JavascriptUncovered,SOverflow,Concolic,RedMonk2017}. Esse sucesso deve-se em parte à sua facilidade de manipulação, pois a linguagem possui uma sintaxe muito flexível, sendo possível atingir um mesmo objetivo utilizando diferentes formas de codificação. Essa flexibilidade é oriunda da tipagem fraca, onde uma variável não é obrigada a ser do mesmo tipo durante toda a execução do código, podendo iniciar como \textit{integer}, por exemplo, e terminar como \textit{string}, \textit{function}, \textit{array} ou outro tipo de objeto. A dinamicidade da linguagem também deve-se à possibilidade de inclusão de trechos de código em tempo de execução \cite{6681338}, permitindo que aplicações desenvolvidas em \textit{JavaScript} executem rotinas de código e percorram fluxos de execução não explicitamente conhecidos.


Essa versatilidade, embora seja útil para o desenvolvimento de aplicações, acaba abrindo um leque de oportunidades para a execução de códigos maliciosos, que podem vir a comprometer os pilares da segurança da informação\footnote{ Os pilares da segurança da informação são formados pela tríade CID (confidencialidade, integridade e disponibilidade)}, como a confidencialidade e a integridade dos dados daqueles que utilizam a aplicação.


Para reduzir o risco de execução de código malicioso, os navegadores modernos fazem uso de um recurso que dificulta o carregamento de código executável a partir de outras fontes, denonimado de Política da Mesma Origem\footnote{ \url{https://www.w3.org/Security/wiki/Same\_Origin\_Policy}}. Por meio dessa política, uma aplicação pode ficar impossibilitada de carregar trechos de códigos de origens que não sejam do seu próprio domínio e até isolar a execução de códigos em diferentes \textit{frames}. Porém, esse recurso somente assegura que códigos de terceiros não possam ser embutidos no código original das aplicações, mas não bloqueia a execução de códigos maliciosos provenientes da própria aplicação.

\section{Motivação e Descrição do Problema}\label{introducao:motivacao}

Com a linguagem \textit{JavaScript} evoluindo continuamente e ganhando novos campos de atuação, como o desenvolvimento de extensões para navegadores \cite{WebFirefox,WebChrome}, para aplicativos \textit{desktop} \cite{WinJS,Electron}, para servidores \cite{Node.js} e para dispositivos móveis \cite{Tizen}, o interesse pelas linguagens dinâmicas ganhou força, fazendo com que a comunidade de pesquisadores se voltasse para a criação de soluções que pudessem resolver problemas de segurança em aberto, tais como o vazamento de informação.

O vazamento de informação é a distribuição acidental ou não intencional de dados sigilosos para um destino não autorizado \cite{Peneti2016}. Esse problema ocorre com maior incidência no universo dos dispositivos móveis \cite{AndroidLeaks}, onde é explorado por aplicações maliciosas que, uma vez instaladas nos dispositivos, capturam dados sigilosos para repassá-los a terceiros.

Diversos trabalhos foram realizados na tentativa de coibir o problema do vazamento de informação sensível. Pinto \textit{et al.} \cite{Nedwons} e Kuzuno \textit{et al.} \cite{6547438} verificam o conteúdo dos pacotes HTTP para checar a existência de informação sigilosa. Wang \textit{et al.}\cite{Wang2014AGL} e Hsiao \textit{et al.} \cite{Hsiao2014PasDroidRS} utilizam listas para permitir ou bloquear o acesso às fontes de informação sensível. Entretanto, a maioria dos trabalhos realiza análise no fluxo da informação, para observar os passos de funcionamento de uma aplicação e descobrir se uma informação sensível está sendo vazada \cite{JSAI,JSNOSE,Concolic,JSWhiz,7372042,Kashyap,6903571,TrustDroid,LeakMiner,Enck2010}.

Contudo, diferente das linguagens estáticas, as quais são bem definidas e fortemente tipadas, a linguagem \textit{JavaScript} torna-se um grande desafio para o desenvolvimento de abordagens que procuram analisar do fluxo da informação. Nesse contexto, os motivos que lhe renderam o título de linguagem de programação mais utilizada do mundo são os mesmos que fazem com que o fluxo da informação seja difícil de ser analisado.

Os trabalhos que procuram detectar o vazamento mediante análise de pacotes HTTP (\textit{Hypertext Transfer Protocol}) não conseguem identificar o vazamento se os dados estiverem ofuscados. Os trabalhos que utilizam listas para permitir ou bloquear o acesso às fontes de informação sensível exigem que o usuário tenha conhecimento técnico o suficiente para decidir o que deve ser permitido e o que deve ser bloqueado. Enquanto os trabalhos que utilizam análise de fluxo esbarram na complexidade computacional do código \textit{JavaScript} devido a sua versatilidade. Por esta razão, muitos desses trabalhos não têm suporte à função \textit{eval}\footnote{É uma função nativa da linguagem \textit{JavaScript} que permite a execução dinâmica de um \textit{script} existente em uma \textit{string}.} ou qualquer outro método de criação dinâmica de código, como ilustra o Código \ref{codeval1}.


\begin{lstlisting}[caption=Exemplo de código \textit{JavaScript} utilizando a função \textit{eval},captionpos=b,label=codeval1]
<script>
  var a = "g=n=>n";
  var b = "g(n-1)";
  var c = "n:1";
  eval(a+'?'+b+'*'+c);
  var saida = g(5);            
</script>
\end{lstlisting}

O Código \ref{codeval1} apresenta um exemplo de código \textit{JavaScript} que utiliza a função \textit{eval} para criar uma função fatorial dinamicamente. Na linha 5 é possível observar uma expressão de concatenação de variáveis \mbox{(a+\enquote*{?}+b+\enquote*{*}+c)} para formar o argumento a ser enviado para a função. Essa concatenação resulta no valor literal \mbox{\enquote*{g=n=>n?g(n-1)*n:1}} que é o argumento enviado à função \textit{eval}, a qual cria a função \textbf{g} em tempo de execução. Ao término da execução da função, a linha seguinte (linha 6) é processada, a qual executa uma operação de atribuição do resultado da função \textbf{g} (criada pela função \textit{eval}) para a variável \textbf{saida}. O valor atribuído à variável \textbf{saida} é 120 que corresponde ao fatorial de 5.

A dificuldade para analisar a execução da função \textit{eval} é o fator determinante para que muitos trabalhos de análise estática de código \textit{JavaScript} não deem suporte à análise de criações dinâmicas de código.

É por esses motivos que este trabalho apresenta uma abordagem que utiliza a análise estática para identificar e prevenir o vazamento de informação sensível em aplicações \textit{web}. O \textit{TaintJSec} consegue verificar o fluxo de código explícito e implícito, acompanha a propagação do \textit{taint tag} na execução da função \textit{eval} e é capaz de identificar o vazamento de informação sensível em códigos ofuscados. A abordagem apresentada nesse trabalho não compromete o desempenho das aplicações por realizar unicamente a análise estática, sendo essa a menos onerosa de todas as técnicas de análise de fluxo existente no estado da arte.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Objetivos}\label{introducao:objetivos}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
\subsection{Objetivo Geral}\label{introducao:objetivogeral}

Aprimorar o processo de detecção de vazamento de dados em código \textit{JavaScript} baseado na análise estática de fluxo de dados.

\subsection{Objetivos Específicos}\label{introducao:objetivosespecificos}

Para alcançar o objetivo geral desta pesquisa, os seguintes objetivos específicos devem ser alcançados:
\begin{enumerate}
\item Desenvolver um método para extrair e agrupar o código \textit{JavaScript} presente na aplicação. É necessário extrair o código \textit{JavaScript} para separá-lo de códigos de outras linguagem, tais como CSS e HTML. O código extraído e agrupado serve de consulta e criação de representações intermediárias que são utilizadas durante a análise estática.
\item Desenvolver um método para criar uma árvore sintática abstrata de um código \textit{JavaScript}. Tal método é utilizado para gerar uma representação intermédiária do código \textit{JavaScript} da aplicação. Essa representação intermediária, no formato de árvore, representará o fluxo de execução do código \textit{JavaScript}.
\item Desenvolver um analisador estático para percorrer a árvore sintática abstrata e realizar as operações contidas em cada nó da árvore.
\item Implantar um mecanismo de marcação de \textit{tags} na estrutura dos registradores de objetos do código \textit{JavaScript} (\textit{taint marking}). Tal mecanismo é embutido no analisador estático, de modo que a marcação seja mais uma das operações realizadas pelo analisador.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Contribuições}\label{conclusao:contribuicoes}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Este trabalho fornece as seguintes contribuições:

\begin{enumerate}

\item Um analisador estático de código \textit{JavaScript} capaz de identificar o vazamento de informação sensível em códigos implícitos e ofuscados.
\item Uma técnica denominada \textit{taint position} para aumentar o controle da propagação do \textit{taint tag} na análise da função \textit{eval}.

\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Estrutura do Documento}\label{introducao:estruturadodocumento}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

O restante desta dissertação está organizada do seguinte modo:
\begin{itemize}
\item O Capítulo \ref{conceitosbasicos} expõe os conceitos básicos e fornece a fundamentação teórica necessária para que o leitor tenha um melhor entendimento sobre o problema tratado neste trabalho.
\item O Capítulo \ref{trabalhos-relacionados} aborda os trabalhos relacionados e as principais abordagens para resolver o problema do vazamento de dados sensíveis. As abordagens são organizadas em três grupos e os trabalhos são apresentados de acordo com o grupo ao qual pertencem. O capítulo também apresenta uma análise comparativa das características e as limitações dos trabalhos relacionados.
\item O Capítulo \ref{proposta} apresenta o \textit{TaintJSec} e descreve as três fases da abordagem. Também apresenta uma nova técnica, denominada \textit{taint position}, para estender o processo de propagação do \textit{taint tag} para o escopo da função nativa \textit{eval}.
\item O Capítulo \ref{testes-resultados} apresenta os testes e os resultados realizados para validar a eficácia do \textit{TaintJSec} na detecção do vazamento de informação sensível. Também são descritas as três baterias de testes: testes de propagação, testes com a função \textit{eval} e testes em códigos ofuscados.
\item O Capítulo \ref{conclusao} conclui este documento apresentando as contribuições, as limitações da abordagem e os trabalhos futuros.
\end{itemize}

\let\cleardoublepage\clearpage





















