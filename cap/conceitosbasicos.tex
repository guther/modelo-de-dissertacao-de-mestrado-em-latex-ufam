\chapter{\textit{JavaScript} e Desenvolvimento \textit{Web}}\label{conceitosbasicos}

Neste capítulo são abordados alguns conceitos básicos, com o propósito de prover ao leitor informações necessárias para que tenha um melhor entendimento sobre o contexto em que este trabalho está inserido. O capítulo inicia com uma breve descrição da linguagem \textit{JavaScript} e conceitua os elementos da linguagem relevantes para os objetivos deste trabalho, como as bibliotecas \textit{JavaScript} e a plataforma \textit{Node.js}. Em seguida, o capítulo apresenta o problema do vazamento de informação e discorre sobre as técnicas de análise de marcação utilizadas para mitigar esse problema.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Linguagem de Programação \textit{JavaScript}}\label{conceitos:JavaScript}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\textit{JavaScript} é uma linguagem de programação comumente utilizada no desenvolvimento \mbox{\textit{web}. Foi} desenvolvida em 1995 pela \textit{Netscape} como um meio para adicionar elementos dinâmicos e interativos para os \textit{sites} \cite{DefJavascript}. Enquanto a \textit{JavaScript} é influenciada pela linguagem Java, sua sintaxe é mais semelhante à linguagem C e baseada na \textit{ECMAScript} \cite{ECMAscript}, uma linguagem de \textit{script} desenvolvida pela \textit{Sun Microsystems}.

\textit{JavaScript} foi originalmente criada para executar no lado do cliente (\textit{client-side}), o que significa que o código-fonte foi projetado para ser processado pelo navegador \textit{web} do cliente e não pelo servidor \textit{web} \cite{JavGuideDef,HTMLArch}. Por esse motivo, as funções de um código \textit{JavaScript} podem ser executadas após uma página \textit{web} ter sido carregada sem se comunicar com o servidor. Por exemplo, uma função  pode verificar um formulário eletrônico antes deste ser enviado para certificar-se de que todos os campos obrigatórios foram preenchidos. Assim, o código \textit{JavaScript} pode produzir uma mensagem de erro antes que qualquer informação seja realmente transmitida para o servidor \cite{W3CJavascript}.  

Somente a partir do ano 2009, com a criação do \textit{Node.js} \cite{Node.js} a linguagem \textit{JavaScript} passou a ser cada vez mais utilizada no lado do servidor (\textit{server-side}). O \textit{Node.js} é um interpretador de código construído sobre o motor \textit{JavaScript} V8 do navegador \textit{Google Chrome} para facilitar a construção de aplicações que executam no \textit{server-side} \cite{NodeJS}.

Assim como as linguagens de \textit{script} que são executadas no \textit{server-side}, tais como PHP (\textit{PHP Hypertext Preprocessor}) e ASP (\textit{Active Server Pages}), o código \textit{JavaScript} pode ser inserido em qualquer lugar dentro do HTML de uma página \textit{web}. Essa inserção pode ser realizada de duas maneiras: \textit{inline} ou por meio de um arquivo externo.

O código \textit{inline} é inserido diretamente no código HTML e o conteúdo do código \textit{JavaScript} fica inteiramente visível entre as \textit{tags <script>} e \textit{</script>}, como exemplifica o Código \ref{indexhtml}, cujo código \textit{inline} está entre as linhas 6 e 15.

\begin{lstlisting}[label=indexhtml,caption=Exemplo de \textit{JavaScript inline}.,captionpos=b]
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Exemplo</title>
        <script>
            function fatorial(n){
            	if(n<0)
            		return -1;
            	else if(n==0)
            		return 1;
            	else return n * fatorial(n-1);
            }
            var resultado = fatorial(4)
        </script>
    </head>
<body>
</body>
</html>
\end{lstlisting}

O código \textit{JavaScript} inserido por um arquivo externo utiliza a \textit{tag <script>} com o atributo \textit{src} (source), cujo valor contém o caminho do código externo que geralmente é referenciado por um arquivo com a extensão .js, mas também pode apresentar outras extensões ou até mesmo nenhuma extensão, pois o documento externo que contém o código \textit{JavaScript} precisa apenas informar ao navegador que o seu conteúdo é um código \textit{JavaScript}, para ser interpretado como tal. Em PHP, por exemplo, essa informação é indicada pela instrução <?php header('Content-type:text/javascript'); ?>. Assim, o arquivo externo pode aparecer com a extensão diferente da tradicional .js e continuar sendo reconhecido pelo navegador como um arquivo \textit{JavaScript}. Essa troca de extensão do arquivo externo é comumente utilizada quando deseja-se que o conteúdo seja gerado em tempo de execução, por meio de uma linguagem de programação dinâmica. A linha 6 do Código \ref{index_libjs} apresenta um exemplo de inserção de código \textit{JavaScript} externo.
\newpage
\begin{lstlisting}[caption=Exemplo de \textit{JavaScript} externo.,captionpos=b,label=index_libjs]
<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8" />
  <title>Exemplo</title>
  <script src="lib.js">
  </script>
 </head>
<body>
</body>
</html>
\end{lstlisting}


Indepentendemente se um código é \textit{inline} ou externo, ele sempre é um código explícito, pois está   declarado em meio à estrutura HTML. Os códigos explícitos são identificados simplesmente olhando o código-fonte da aplicação. Por exemplo, o Código \ref{indexhtml} contém código \textit{JavaScript} entre as linhas 6 e 15, enquanto o Código \ref{index_libjs} contém código \textit{JavaScript} entre as linhas 6 e 7.

Por outro lado, os códigos implícitos são difíceis de identificar visualmente, pois ficam ocultos na lógica de execução dos códigos explícitos. Geralmente, esse tipo de código é armazenado em variáveis, assumindo inicialmente valores literais (\textit{strings}). O código implícito depende da execução de um código explícito para que seja transformado em um novo elemento \textit{script} anexado ao DOM (\textit{Document Object Model}). Dentre as funções mais utilizadas para anexar novos trechos de código ao DOM destacam-se a \textit{document.write}, \textit{appendChild} e a função \textit{eval}.

O Código \ref{codigo_implicito} apresenta um código HTML que contém código \textit{JavaScript} com códigos implícitos em sua lógica de execução.

\begin{lstlisting}[caption=Trecho de código explícito contendo códigos implícitos.,captionpos=b,label=codigo_implicito]
<!DOCTYPE html>
<html>
  <head>
		<script>
		 var implicito_1 = "<script>alert('teste1');</"+"script>";
		 document.write(implicito_1);
		 
		 var implicito_2 = "<script src='lib.js'></"+"script>";
		 document.write(implicito_2);
		 
		 var implicito_3 = document.createElement("script");
		 implicito_3.src = "lib.js";
		 document.body.appendChild(implicito_3);
		</script>
  </head>
</html>
\end{lstlisting}

É possível observar na linha 5 que a variável \textit{implicito\_1} é iniciada com um valor literal que não é um código \textit{JavaScript} reconhecido pelo DOM, mas passa a ser considerado um novo trecho de código após a execução da função \textit{document.write} da linha 6. O mesmo ocorre com a variável \textit{implicito\_2}, porém esta difere-se por não trazer consigo códigos \textit{inline}, mas faz referência a um arquivo externo. Já a variável \textit{implicito\_3}, na linha 11, não é declarada com um valor literal, mas com um novo elemento do HTML que, inicialmente, não faz parte do DOM, vindo a fazer parte após a execução da função \textit{appendChild}, na linha 13.

A Figura \ref{fig:codigo_implicito_criado} apresenta o resultado da execução do Código \ref{codigo_implicito}, onde outras \textit{tags <script>} foram dinamicamente criadas.

\begin{figure}[h] % [b] (de bottom) é para colocar a figura ao final da página; [t] (de top)  é para o topo da página
    \centering
    \includegraphics[scale=.7]{img/fig_cod_implicito_criado} % caso queira mudar o tamanho da figura
    \caption{Novos códigos criados dinamicamente.}
    \label{fig:codigo_implicito_criado}
\end{figure}

A Figura \ref{fig:codigo_implicito_criado} apresenta em destaque três novos trechos de códigos \textit{JavaScript} criados dinamicamente, em tempo de execução. Tais códigos estavam implícitos na lógica de execução dos códigos explícitos. Ao observar o Código \ref{codigo_implicito} não é possível ver os novos trechos de código tal como apresentados na Figura \ref{fig:codigo_implicito_criado}, pois eles somente passam a existir após a execução dos códigos explícitos. Porém, ainda é possível ver que as variáveis \textit{implicito\_1} e \textit{implicito\_2} recebem blocos de código \textit{JavaScript} em forma de \textit{string}.

Essa visualização só é possível porque os valores atribuídos às variáveis \textit{implicito\_1} e \textit{implicito\_2} estão em texto plano\footnote{ Um texto simples com formato humanamente legível, sem qualquer embaralhamento proposital para dificultar a leitura.}, mas caso estivessem ofuscados não seria possível identificar, apenas olhando para o código, que as variáveis estavam recebendo blocos de código implícito, como exemplifica o Código \ref{codigo_implicito_ofuscado}.

\begin{lstlisting}[caption=Trecho de código explícito contendo códigos implícitos ofuscados.,captionpos=b,label=codigo_implicito_ofuscado]
<!DOCTYPE html>
<html>
  <head>
		<script>
		 var implicito_1 = "\x3C\x73\x63\x72\x69\x70\x74\x3E\x61\x6C\x65\x72\x74\x28\x27\x74\x65\x73\x74\x65\x31\x27\x29\x3B\x3C\x2F\x73\x63\x72\x69\x70\x74\x3E";
		 document.write(implicito_1);
		 
		 var implicito_2 = "\x3C\x73\x63\x72\x69\x70\x74\x20\x73\x72\x63\x3D\x27\x6C\x69\x62\x2E\x6A\x73\x27\x3E\x3C\x2F\x73\x63\x72\x69\x70\x74\x3E";
		 document.write(implicito_2);
		 
		 var implicito_3 = document.createElement("script");
		 implicito_3.src = "lib.js";
		 document.body.appendChild(implicito_3);
		</script>
  </head>
</html>
\end{lstlisting}

O Código \ref{codigo_implicito_ofuscado} é uma cópia Código \ref{codigo_implicito}, porém os valores atribuídos às variáveis \textit{implicito\_1} e \textit{implicito\_2}, que antes eram textos planos, foram substituídos por sua representação hexadecimal. Os valores que as variáveis recebem são os mesmos em ambos os códigos, a única diferença é que no Código \ref{codigo_implicito_ofuscado} os valores estão ofuscados.

Código ofuscado é o código que passou por um processo de ofuscação, ou seja, um embaralhamento com o objetivo de transformar o código legível em um equivalente que é mais difícil de entender \cite{SEC:SEC1064,jodavi2015}.

Assim, é possível notar que a linguagem \textit{JavaScript} é bastante flexível para permitir que um mesmo resultado possa ser obtido de diferentes maneiras. A possibilidade de criar códigos dinamicamente permite maior interatividade com a página \textit{web}. Enquanto que criar códigos ofuscados é comumente usado por desenvolvedores de \textit{software} para proteger a propriedade intelectual de seu código.

Contudo, a versatilidade da linguagem \textit{JavaScript} também é explorada para a criação de \textit{malwares}, que utilizam os recursos da linguagem para evadir sistemas de detecção baseados em assinaturas e praticar o vazamento de informação.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Vazamento de Informação}\label{conceitos:vazamentoinformacao}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Vazamento de informação é definido como uma distribuição acidental ou não intencional de dados sigilosos para um destino não autorizado \cite{Peneti2016}.

Tal vazamento pode facilmente acarretar prejuízos ao dono da informação. Dependendo do tipo de informação vazada, o dano causado pode ser financeiro ou moral. Uma empresa, por exemplo, que possui muitas informações sigilosas, como dados financeiros, números de cartão de crédito e planos estratégicos, pode sofrer prejuízos caso esses dados sejam enviados à uma entidade não autorizada. Como foi o caso da empresa Sony no ano de 2014, onde \textit{crackers} roubaram uma grande quantidade de informações sigilosas e divulgaram na Internet cinco filmes inéditos, causando um prejuízo financeiro estimado em US\$ 300 milhões \cite{CasoSony}.

Os danos causados por vazamento de informação privilegiada podem ser classificados em duas categorias: diretos e indiretos.

Os danos diretos referem-se a danos tangíveis, ou seja, são danos mensuráveis, fáceis de medir ou estimar quantitativamente. Os danos indiretos, por outro lado, são muito mais difíceis de quantificar e possuem um impacto muito mais amplo em termos de custo, lugar e tempo \cite{5487521}. Geralmente o dano indireto é identificado quando a empresa tem a sua imagem prejudicada junto aos seus clientes, fazendo com que novos negócios deixem de ser firmados devido à insegurança criada após um evento de vazamento de dados sigilosos. Como foi o caso da empresa Uber, que teve dados de 57 milhões de contas de usuários vazados no ano de 2016 quando \textit{crackers} invadiram seus servidores. A empresa manteve segredo durante um ano, vindo a revelar sobre o vazamento no final do ano de 2017, quando havia acabado de fazer um acordo com a procuradoria-geral de Nova York sobre um processo envolvendo segurança de dados \cite{casoUber}.

Com as pessoas, o dano indireto é identificado quando o vazamento de informação prejudica a moral ou a honra, fazendo com que a pessoa sinta-se desmoralizada, envergonhada ou prejudicada junto à sociedade.

O risco de ocorrer o vazamento de informação nas empresas está presente desde a perda de um malote de documentos impressos até uma invasão de \textit{crackers} nos servidores. Porém, com o aumento da prática de BYOD (\textit{Bring Your Own Device}) o risco de vazamento encontra-se também nos dispositivos das pessoas.

No BYOD, um funcionário pode levar seu \textit{laptop}, \textit{tablet} ou \textit{smartphone} para a empresa e utilizá-lo como uma ferramenta de trabalho, acessando a rede coorporativa e tendo acesso a dados restritos \cite{BYOD}. Um funcionário com acesso a informações da empresa pode copiá-las para seu dispositivo ou para um repositório na nuvem, como o \textit{Dropbox}\footnote{ \url{http://www.dropbox.com}} por exemplo. Esse dispositivo está sujeito a infecções de vírus, \textit{spywares}, extravio e até mesmo a instalação de aplicações maliciosas, as quais não são detectadas por sistemas antivírus.

Na tentativa de mitigar o problema do vazamento de informação em dispositivos, muitos trabalhos foram realizados. Em sua maioria, encontram-se os trabalhos que utilizam análise do fluxo de execução de aplicações instaladas nos dispositivos, a fim de detectar se uma aplicação está causando vazamento de dados sigilosos.

A seção \ref{conceitos:analisemarcacao} aborda sobre a técnica de análise de marcação em código, a qual é uma técnica que observa como os dados sensíveis transitam na aplicação.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Análise de Marcação (\textit{Taint Analysis})}\label{conceitos:analisemarcacao}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

A análise do fluxo da informação é utilizada por métodos que fazem a verificação dos passos de funcionamento de uma aplicação.
A técnica consiste em marcar os dados que são identificados como dados sensíveis e observar por onde esses dados marcados (\textit{tainted data}) são propagados. Permitindo assim identificar o momento que um \textit{tainted data} propaga-se de um objeto para outro (\textit{taint propagation}).

Essa verificação pode ser realizada dinamicamente em tempo de execução (análise dinâmica) ou pode ocorrer em modo estático durante o processo de compilação do código (análise estática).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Análise de Marcação Dinâmica}\label{conceitos:analisedinamica}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

A análise dinâmica de código é uma técnica que faz suas verificações em tempo de execução \cite{7888731,JSNOSE,Saoji}, com o programa em funcionamento. Essa análise é realizada em um ambiente controlado e permite que o usuário interaja com a aplicação com segurança, pois parte da premissa de que um código malicioso pode ser ativado especificamente após certo evento ter ocorrido no sistema \cite{6234407}.

A análise funciona por meio de modificações realizadas no código original do programa, onde métodos utilizados para rastrear a informação são inseridos em pontos estratégicos do código. Assim, todos os dados que estiverem trafegando durante a execução do programa são analisados.

Ao acompanhar cada passo de um dado no fluxo da aplicação é possível agir com maior justiça, reduzindo os números de falsos positivos. Por outro lado, a sobrecarga imposta (\textit{overhead}) para a execução da análise dinâmica é superior ao da análise estática \cite{Yong2005}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Análise de Marcação Estática}\label{conceitos:analiseestatica}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

A análise estática de código é um método utilizado para verificar o comportamento de um programa sem que seja necessário executá-lo \cite{7977127}. A análise consiste em percorrer o código, tratando-o como um grafo dirigido, o qual representa o fluxo de execução do programa. Nesse grafo, a análise verifica a operação contida em cada vértice a fim de decidir qual aresta será utilizada para continuar o caminho pelo grafo.

Esse grafo é obtido por um processo de transformação do código-fonte em numa estrutura
de dados, geralmente designada por uma árvore sintática abstrata (AST - \textit{Abstract Syntax Tree}), usando um conjunto de regras predeterminado \cite{Asund}.

A árvore sintática abstrata ou simplesmente AST é uma representação sintática simplificada do código-fonte e, na maioria das vezes, é expressa em uma estrutura de dados da linguagem de programação utilizada para a implementação. É uma estrutura em formato de árvore, onde cada nó dessa árvore denota uma instrução que ocorre no código \cite{Mohammed_A}. A sintaxe é \enquote{abstrata} (simplificada) por não representar todos os detalhes que aparecem na sintaxe real do código \textit{JavaScript}. Por exemplo, o agrupamento de parênteses está implícito na estrutura da árvore e uma construção sintática como uma expressão \textit{IF} pode ser apresentada como sendo um único nó com três ramificações. Isso distingue a AST de uma árvore de sintaxe concreta, tradicionalmente nomeada como árvore de análise ou árvore de \textit{parser}, muitas vezes construída por um analisador durante o processo de compilação de códigos-fonte. Logo, a AST não tem necessidade de ter tanta informação quanto a árvore de \textit{parser}.

A Figura \ref{fig:ast} exemplifica um simples código em \textit{JavaScript} e sua respectiva representação em formato de árvore sintática abstrata.

\begin{figure}[h] % [b] (de bottom) é para colocar a figura ao final da página; [t] (de top)  é para o topo da página
    \centering
    \includegraphics[scale=.7]{img/fig_ast} % caso queira mudar o tamanho da figura
    \caption{Exemplo de um código \textit{JavaScript} com sua respectiva AST.}
    \label{fig:ast}
\end{figure}

A AST é usada intensivamente por compiladores, durante a análise semântica de um código-fonte, onde o compilador verifica o uso correto dos elementos do programa e da linguagem, e depois de verificar a corretude, a AST serve como base para a criação de uma representação intermediária durante a compilação do código.

Além dos compiladores, a AST também é utilizada em outros campos de análise de código como, por exemplo, os trabalhos \cite{5705174,kukushi2014, lazar2014} que utilizam a AST como uma representação intermediária para verificar a existência de plágio entre dois ou mais códigos-fontes. O trabalho de Neamtiu \textit{et al.} \cite{Neamtiu2005} usa a AST para verificar a evolução de um determinado código e apresenta as diferenças entre suas versões. Já os trabalhos de Blanc \textit{et al.} \cite{blanc2012} e Curtsinger \textit{et al.} \cite{Curtsinger2011} utilizam a AST para detectar códigos maliciosos escondidos em trechos ofuscados de \textit{JavaScript}.

Atualmente existem ferramentas voltadas para a realização do processo de transformação de código-fonte em AST como, por exemplo, o Esprima \cite{Esprima}. O Esprima é um módulo do \textit{Node.js} utilizado para gerar estruturas de dados a partir de códigos \textit{JavaScript}.

A Figura \ref{fig_ast_esprima} apresenta um exemplo de um código \textit{JavaScript} e a sua respectiva AST gerada pelo módulo Esprima.

\begin{figure}[h]
    \centering
    \includegraphics[scale=.6]{img/fig_ast_esprima.png}
    \caption{Exemplo de AST gerada pelo módulo Esprima a partir de um código \textit{JavaScript}.}
    \label{fig_ast_esprima}
\end{figure}

A Figura \ref{fig_ast_esprima} apresenta o código \textit{JavaScript} \textbf{var answer = 6 * 7;} que, ao ser enviado ao módulo Esprima, resultou na sua representação em árvore sintática abstrata. A árvore é fornecida no formato JSON (\textit{JavaScript Object Notation}) e cada um de seus vértices possuem o atributo \textit{type} para informar o tipo de operação executada em suas subárvores.

Por meio das informações contidas nos vértices da AST, a análise estática decide quais operações realizar e qual a próxima aresta que será utilizada para continuar a análise.

Quando uma operação de fornecimento de dados sensíveis é detectada, a análise marca o objeto (\textit{taint marking}) que passa a conter o dado. Esse objeto, agora marcado pela análise, é continuamente observado para que os vértices, que contenham operações que interajam com esse objeto, sejam tratados como potenciais pontos de propagação de informação sensível (\textit{taint source}). Todos os caminhos do grafo que terminam em uma operação de saída (\textit{taint sink}) são considerados caminhos de vazamento de informação em potencial.

Em comparação com a análise dinâmica, a análise estática possui menor consumo de memória e, se utilizada em dispositivos móveis, consome menos bateria, pois não precisa acompanhar a sequência de fluxo de dados a cada interação durante todo o tempo em que o programa estiver em funcionamento \cite{TrustDroid}. Além disso, a análise estática permite percorrer todos os caminhos possíveis do grafo de execução de uma aplicação logo na primeira análise \cite{Saoji}. Diferente da análise dinâmica que necessita de um evento específico para analisar cada caminho do fluxo de execução. Ainda mais, a análise estática não necessita de intervenções no código-fonte original dos sistemas para que possa ser aplicada. Já a análise dinâmica necessita de alterações no código-fonte dos sistemas hospedeiros.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Termos da Análise de Marcação}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Aqui são apresentados alguns termos da análise de marcação utilizados neste trabalho.

\medskip\textbf{\textit{Taint Tag}}

É um atributo de valor booleano que indica se um objeto está marcado ou não. Quando o valor booleano é positivo, significa que o objeto está marcado. 

\medskip\textbf{\textit{Taint Data}}

É o nome dado a um objeto marcado com \textit{taint tag} positivo.
Os \textit{tainted data} são objetos que contêm informações sensíveis.

\medskip\textbf{\textit{Taint Marking}}

É o método de marcação dos objetos \cite{Enck2010}. Cada abordagem que utiliza \textit{Taint Analysis} cria seu próprio processo de \textit{taint marking}.

\newpage
\medskip\textbf{\textit{Source}}

É um fornecedor de informações, que pode ser um método, função, objeto ou atributo de um objeto nativo que fornece alguma informação para o programa. Em \textit{JavaScript}, por exemplo, são considerados \textit{sources} os objetos como \textit{window.navigator} e \textit{window.history}, onde um fornece informações sobre o navegador do usuário e o outro fornece informações sobre o histórico de navegação, respectivamente.

\medskip\textbf{\textit{Taint Source}}

É um fornecedor de informações, assim como o \textit{source}, mas é um fornecedor marcado com \textit{taint tag} positivo, por isso é denominado de \textit{taint source}. Essa marcação é utilizada para diferenciar os fornecedores de dados sensíveis, \textit{taint sources}, dos demais \textit{sources} que não fornecem qualquer dado sensível \cite{DBLP:journals/corr/LiBKTARBOM14}.

\medskip\textbf{\textit{Taint Sink}}

É um sumidouro também referenciado simplesmente de \textit{sink}, que pode ser uma função, método ou objeto nativo que possui a capacidade de enviar um dado para fora dos domínios do programa \cite{DBLP:journals/corr/LiBKTARBOM14, Enck2010}. Em \textit{JavaScript}, por exemplo, são considerados \textit{sinks} o objeto \textit{window.localStorage} e a função \textit{window.postMessage()}, onde um é utilizado para armazenar dados no navegador do usuário e o outro serve para enviar mensagens para outros domínios, respectivamente. Durante a análise, o \textit{sink} é sempre o último vértice do grafo de execução de código, antes que o vazamento de informação seja detectado, servindo como porta de saída para um \textit{tainted data}.

\medskip\textbf{\textit{Taint Propagation}}

É a propagação de um objeto com \textit{taint tag} negativo por um \textit{tainted data}. A propagação do \textit{taint tag} ocorre em diversos momentos da execução do código, como em operações de atribuição, expressões binárias ou passagem de parâmetros, por exemplo.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Considerações Finais}\label{conceitos:consideracoes}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Este capítulo forneceu conceitos necessários para um melhor entendimento sobre o problema tratado nesta pesquisa. A linguagem de programação \textit{JavaScript} foi apresentada com uma breve descrição de suas principais características, tais como bibliotecas de desenvolvimento, código \textit{inline}, código externo, código explícito e implícito. O problema do vazamento de informação foi descrito, informando os prejuízos causados por ele, que classificam-se em danos diretos e indiretos. A técnica de mitigação de vazamento de informação, conhecida como Análise de Marcação, foi explanada discorrendo sobre os dois tipos de análise de fluxo da informação existentes: análise dinâmica e análise estática.

Neste capítulo também foi realizada uma breve apresentação da plataforma de desenvolvimento \textit{Node.js}, que utiliza a linguagem \textit{JavaScript} para executar no lado do servidor, e os módulos jsDom e Esprima, que têm função semelhante às bibliotecas \textit{JavaScript} e são utilizados durante a implementação deste trabalho (Seção \ref{proposta:implementacao}).

\newpage

