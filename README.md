# Modelo de Disserta��o de Mestrado em LaTeX

Esse modelo de disserta��o � uma modifica��o do modelo criado por Mirella M. Moro, que pode ser encontrado em http://homepages.dcc.ufmg.br/~mirella/doku.php?id=alunos

Agrade�o a ela por ter compartilhado seu modelo em LaTeX e agora sigo seu gesto e compartilho com os senhores o meu modelo.

Alexandre B. Damasceno

-----------------------------------------


1� - Instale o editor de LaTeX de sua prefer�ncia. Eu recomendo o uso do TexMaker. http://www.xm1math.net/texmaker/

2� - Baixe e instale o Miktex. https://miktex.org/download
Durante a instala��o, o instalador do Miktex ir� perguntar se voc� deseja que os pacotes LaTeX sejam instalados em tempo de execu��o, marque para que voc� seja questionado. Assim voc� ir� ter a certeza de que o arquivo LaTeX estar� sendo compilado.

3� - Instale um leitor de PDF. No Windows, recomendo o Acrobat Reader. Ap�s a instala��o, � preciso inserir o caminho do leitor de PDF nas op��es do editor LaTeX.

4� - Versione/baixe os arquivos do modelo.

5� - Esse modelo necessita das fonte "Noto Mono" e "Noto Sans Mono JCK JP" instaladas. Baixe-as e instale-as. 
Noto Mono - https://noto-website-2.storage.googleapis.com/pkgs/NotoMono-hinted.zip
Noto Sans Mono JCK JP - https://noto-website-2.storage.googleapis.com/pkgs/NotoSansCJKjp-hinted.zip

6� - Abra o arquivo "dissertacao.tex" e marque esse arquivo como "master" do projeto. No TexMaker, essa op��o fica em: Op��es -> Definir o documento atual como "Documento Mestre".

7� - Compile o arquivo usando a op��o "XeLaTex".



